﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization;

namespace SpriteWatcher
{
    public partial class Form1 : Form
    {
        System.IO.FileSystemWatcher fileWatcher;
        String fileName;
        PaintDotNet.Document d;
        internal Timer timer;
        bool needReload = false;
        int currentFrame = 0;
        FormSettings settings;
        Bitmap bm1;
        Bitmap bm1Clone;
        Rectangle currentFrameRect;

        public Form1()
        {
            InitializeComponent();

            PaintDotNet.Document.Initialize(new Version(10, 1, 5, 2));
            PaintDotNet.Document doc = new PaintDotNet.Document(128, 128);
            //PaintDotNet.BitmapLayer layer = PaintDotNet.Layer.CreateBackgroundLayer(128, 128, "test");
            //doc.Layers.Add(layer);

            using (FileStream fs = new FileStream("test.pdn", FileMode.Create))
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bin = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                bin.Serialize(fs, doc);
            }

            timer = new Timer();
            timer.Tick += t_Tick;
            timer.Interval = 1000;
            timer.Start();

            settings = new FormSettings(this);
            this.propertyGrid1.SelectedObject = settings;

            this.pictureBoxSprite.Paint += pictureBoxSprite_Paint;
            this.pictureBoxPreview.Paint += pictureBoxPreview_Paint;
        }

        void pictureBoxPreview_Paint(object sender, PaintEventArgs e)
        {
            if (bm1 != null)
            {
                lock (bm1)
                {
                    Point previewTopLeft = new Point((int)Math.Floor(e.ClipRectangle.Width / 2f - bm1.Width / 2f), (int)Math.Floor(e.ClipRectangle.Height / 2f - bm1.Height / 2f));

                    Rectangle highlightRect;




                    Pen highlightPen = new Pen(Color.Black);
                    for (int i = 0; i < settings.Frames; i += 1)
                    {
                        highlightRect = new Rectangle(settings.XOffset + i * (settings.Width + settings.XBorder),
                            settings.YOffset, settings.Width, settings.Height);
                        while (highlightRect.X >= d.Width) { highlightRect.Y += settings.Height; highlightRect.X -= d.Width; }

                        highlightRect.X += previewTopLeft.X;
                        highlightRect.Y += previewTopLeft.Y;

                        e.Graphics.DrawRectangle(highlightPen, highlightRect);
                    }

                    System.Drawing.Drawing2D.HatchBrush currentFrameBrush = new System.Drawing.Drawing2D.HatchBrush(System.Drawing.Drawing2D.HatchStyle.WideUpwardDiagonal, Color.Black, Color.White);
                    Pen currentFramePen = new Pen(currentFrameBrush);
                    highlightRect = new Rectangle(settings.XOffset + currentFrame * (settings.Width + settings.XBorder),
                            settings.YOffset, settings.Width, settings.Height);
                    while (highlightRect.X >= d.Width) { highlightRect.Y += settings.Height; highlightRect.X -= d.Width; }
                    highlightRect.X += previewTopLeft.X;
                    highlightRect.Y += previewTopLeft.Y;

                    e.Graphics.DrawRectangle(currentFramePen, highlightRect);

                    e.Graphics.DrawImage(bm1, previewTopLeft);
                }
            }
        }

        void pictureBoxSprite_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                if (bm1Clone != null)
                {
                    lock (bm1Clone)
                    {
                        if (bm1Clone != null && currentFrameRect != Rectangle.Empty)
                        {
                            e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                            e.Graphics.DrawImage(bm1Clone, new Rectangle(e.ClipRectangle.Width / 2 - settings.Width * settings.Zoom / 2,
                                e.ClipRectangle.Height / 2 - settings.Height * settings.Zoom / 2, settings.Width * settings.Zoom, settings.Height * settings.Zoom), currentFrameRect, GraphicsUnit.Pixel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO multi thread support for bm1
            }
        }

        void t_Tick(object sender, EventArgs e)
        {
            if (needReload) { needReload = false; this.ReloadImage(); }
            if (d != null)
            {
                currentFrame += 1;
                if (currentFrame >= settings.Frames) { currentFrame = 0; }

                currentFrameRect = new Rectangle(settings.XOffset + currentFrame * (settings.Width + settings.XBorder),
                    settings.YOffset, settings.Width, settings.Height);
                while (currentFrameRect.X >= d.Width) { currentFrameRect.Y += settings.Height; currentFrameRect.X -= d.Width; }

                RenderSprite(currentFrameRect);
            }
        }

        private void buttonLoadImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.InitialDirectory = Directory.GetCurrentDirectory();
            fd.Filter = "PDN Images|*.pdn";
            if (fd.ShowDialog(this) == DialogResult.OK)
            {
                fileName = fd.FileName;
                if (fileWatcher != null) { fileWatcher.Dispose(); }

                fileWatcher = new FileSystemWatcher(Path.GetDirectoryName(fd.FileName));
                fileWatcher.Filter = Path.GetFileName(fd.FileName);
                fileWatcher.Changed += new FileSystemEventHandler(fileWatcher_Changed);
                fileWatcher.EnableRaisingEvents = true;
                ReloadImage();
            }
        }

        void fileWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            needReload = true;
        }

        void ReloadImage()
        {
            if (fileWatcher != null)
            {
                using (FileStream f = File.Open(fileName, FileMode.Open, FileAccess.Read))
                {
                    d = PaintDotNet.Document.FromStream(f);
                }

                this.checkedListBox1.Items.Clear();

                foreach (PaintDotNet.Layer l in d.Layers)
                {
                    this.checkedListBox1.Items.Add(new LayerContainer(l), l.Visible);
                }

            }
            RenderPreview();
        }

        void RenderSprite(Rectangle toRender)
        {
            this.pictureBoxSprite.Invalidate();
            this.pictureBoxPreview.Invalidate();
        }

        void RenderPreview()
        {
            PaintDotNet.Surface s = new PaintDotNet.Surface(d.Width, d.Height);
            d.Render(new PaintDotNet.RenderArgs(s), true);
            if (bm1 != null) { bm1.Dispose(); }
            bm1 = s.CreateAliasedBitmap(true);
            if (bm1Clone != null) { bm1Clone.Dispose(); }
            bm1Clone = (Bitmap)bm1.Clone();
            //if (this.pictureBoxPreview.BackgroundImage != null) { this.pictureBoxPreview.BackgroundImage.Dispose(); }
            //this.pictureBoxPreview.BackgroundImage = bm1;
            //this.pictureBoxPreview.BackgroundImageLayout = ImageLayout.Center;

            this.pictureBoxPreview.Invalidate();
            //this.pictureBoxPreview.Refresh();

        }

        void checkedListBox1_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
        {
            ((LayerContainer)this.checkedListBox1.Items[e.Index]).l.Visible = e.NewValue == CheckState.Checked;
            RenderPreview();
        }

        internal Color GetBackgroundColor()
        {
            return this.pictureBoxPreview.BackColor;
        }

        internal void SetBackgroundColor(Color color)
        {
            this.pictureBoxPreview.BackColor = color;
            this.pictureBoxSprite.BackColor = color;
        }
    }

    public class FormSettings
    {
        public FormSettings(Form1 parent)
        {
            this.parent = parent;
            this.Width = 32;
            this.Height = 32;
            this.Frames = 1;
            this.FPS = 20;
            this.Zoom = 2;

        }

        Form1 parent;

        public int Width { get; set; }
        public int Height { get; set; }
        public int Frames { get; set; }
        public float FPS
        {
            get { return 1000 / parent.timer.Interval; }
            set { parent.timer.Interval = (int)(1000 / value); }
        }
        public int Zoom { get; set; }
        public int XOffset { get; set; }
        public int YOffset { get; set; }
        public int XBorder { get; set; }
        public int YBorder { get; set; }
        public String CustomFrameSequence { get; set; }
        public Color backgroundColor
        {
            get { return parent.GetBackgroundColor(); }
            set { parent.SetBackgroundColor(value); }
        }
    }

    public class LayerContainer
    {
        internal PaintDotNet.Layer l;
        public LayerContainer(PaintDotNet.Layer l) { this.l = l; }
        public override string ToString()
        {
            return l.Name;
        }
    }
}

